<!DOCTYPE html>

<!--  Mode: HTML -->

<!-- CDR-features.tex -->

<!--
\documentclass{article}


\usepackage{latexsym}
\usepackage{epsfig}
\usepackage{alltt}


\usepackage{titlesec}


\newcommand{\tm}{$^\mathsf{tm}$}
\newcommand{\cfr}{\emph{cfr.}}

\newcommand{\Lisp}{\textsf{Lisp}}
\newcommand{\CL}{\textsf{Common~Lisp}}

\newcommand{\Java}{\textsc{Java}}

\newcommand{\checkcite}[1]{{\textbf{[Missing Citation: #1]}}}
\newcommand{\checkref}[1]{{\textbf{[Missing Reference: #1]}}}

\newcommand{\missingpart}[1]{{\ }\vspace{2mm}\\
{\textbf{[Still Missing: #1]}}\\
\vspace{2mm}}

\newcommand{\marginnote}[1]{%
\marginpar{\raggedright \begin{small}\begin{em}#1
\end{em}\end{small}}}

\addtolength{\oddsidemargin}{-2cm}
\addtolength{\evensidemargin}{-2cm}
\addtolength{\textwidth}{4cm}



%%% CL 

\newcommand{\code}[1]{\texttt{#1}}

\newcommand{\term}[1]{\texttt{#1}}
\newcommand{\nonterm}[1]{\textit{$<$#1$>$}}


\newcommand{\kwd}[1]{\texttt{:#1}}
\newcommand{\variable}[1]{\textit{#1}}

\newcommand{\clglossary}[1]{\emph{#1}}

\newcommand{\CPL}{\subsubsection*{Class Precedence List:}}
\newcommand{\Syntax}{\subsubsection*{Syntax:}}
\newcommand{\ArgsValues}{\subsubsection*{Arguments and Values:}}
\newcommand{\Description}{\subsubsection*{Description:}}
\newcommand{\Examples}{\subsubsection*{Examples:}}
\newcommand{\AffectedBy}{\subsubsection*{Affected by:}}
\newcommand{\Exceptions}{\subsubsection*{Exceptional Situations:}}
\newcommand{\SeeAlso}{\subsubsection*{See Also:}}
\newcommand{\Notes}{\subsubsection*{Notes:}}


%%% Document specific macros.

\newcommand{\heap}{\texttt{heap}}
\newcommand{\heapfinger}{\texttt{heap-finger}}
-->

<html>
  <head>
    <title>Detecting Implementation of CDR's in Common Lisp
      Runtimes</title>
  </head>

  <body>

    <h1>Detecting Implementation of CDR's in Common Lisp Runtimes</h1>

    <p>Marco  Antoniotti<br />Universit&agrave; degli Studi di Milano Bicocca,
      Milan, Italy<br /><code>&lt;marco.antoniotti [a]
        unimib.it&gt;</code></p>

    <p><b>Keywords:</b> Common Lisp, CDR, Implementation Features.</p>


    <h1>Introduction</h1>

    <p>The <em>Common Lisp Document Repository</em> (CDR)
      [<a href='#CDRsite'>2</a>] was created as a very light-weight
      infrastructure for the Common Lisp community, where a number of
      &quot;documents&quot; and &quot;specifications&quot; are collected
      and <em>fixed</em> for the benefit of programmers and implementors.
      Each document is give an unique <em>CDR identifier</em> (essentially
      a number), which is retained over the years; each of these documents
      can then be referred simply as <i>CDR number N</i> (or, more simply,
      <i>a CDR</i>, when not referring to a particular document in the
      repository).</p>

    <p>At the time of this writing, there is yet no agreed upon way to
      check whether a <strong>Common Lisp</strong> implementation provides
      a particular CDR or not (i.e., whether a particular CDR is present
      &quot;out of the box&quot;, or whether a library implementing a
      specific CDR is loaded in the <strong>Common Lisp</strong>
      environment).  The goal of this document is to provide a
      specification for this behavior.</p>


    <h2>Rationale</h2>

    <p>Each CDR is assigned a unique number/identifier.  It therefore
      appears natural to resort to the <strong>Common Lisp</strong>
      <code>*features*</code> machinery to provide a minimal
      infrastructure to check for the presence of a given CDR in a
      <strong>Common Lisp</strong> environment.  To do so, a few
      definitions are necessary and will be listed in the next
      section.</p>


    <h1>Specification</h1>

    <p>The specification contained in this document consists of the
      following items:
      <ol>
        <li>As per CDR n.&nbsp;0 and CDR n.&nbsp;4, each document
      submitted to the CDR editors is assigned a unique number; form
      now on it will also be assigned a
          <em><a href='http://www.lispworks.com/documentation/HyperSpec/Body/26_glo_k.htm'>keyword</a></em>
          of the form <code>:cdr-</code><em>n</em> (where <em>n</em> is
          the unique CDR number assigned by the editors).
          The numer <em>n</em> is a <code>(integer 0)</code>, if
          <strong>Common Lisp</strong> types are to be
          used, and its typographical representation is as if it were
          <i><code>print</code>ed</i> with <code>*PRINT-BASE*</code> set
          to 10.</li>

        <li>An <em>implementation</em> of a given CDR (say, CDR 42) should
          provide the appropriate keyword (say, <code>:cdr-42</code>) in the
          <code>*features*</code> list.</li>

        <li>If a given <code>:cdr-</code><em>n</em> is present in
          the <code>*features*</code> list of a given <strong>Common
          Lisp</strong> environment, that means only that
          <blockquote>
            specific instance of a <strong>Common Lisp</strong>
            environment <em>purports to implement</em>
            CDR&nbsp;<em>i</em> at a &quot;satisfactory&quot; level of
            compliance.
        </blockquote></li>
      </ol>
    </p>

    <p>Users and programmers can thus check whether a give CDR is
      &quot;present&quot; in a <strong>Common Lisp</strong> environment,
      using the usual <code>*features*</code> checking machinery.</p>


    <h2>Guarantees, Non-guarantees etc.&nbsp;etc.</h2>

    <p>It must be noted that there are possible pitfalls that the
      &quot;CDR process and infrastructure&quot; cannot avoid.  In the
      following they are listed in no particular order.</p>

    <h3>&quot;Purports to implement&quot;</h3>

    <p>The &quot;CDR process and infrastructure&quot; cannot guarantee
      that the presence of a <em>CDR keyword</em> in a <strong>Common
        Lisp</strong> environment <code>*features*</code> list corresponds
      to a &quot;correct&quot; and &quot;complete&quot; implementation of
      a given CDR.  &quot;Correctness&quot;, &quot;completeness&quot; and
      &quot;testing&quot; are left to the &quot;provider&quot; of a given
      CDR.</p>

    <p>It is understood that a provider of a given CDR (a provider who
      <em>purports to implement...</em>) will make a best effort to fully
      implement a specification.</p>


    <h3>Multiple Implementations of a Given CDR</h3>

    <p>It may be possible for <em>multiple</em> implementations of a given
      CDR to co-exist in a given <strong>Common Lisp</strong> environment.
      All of them will rely on a single <code>:cdr-</code><em>n</em> in
      the <code>*feature*</code> list.  Which particular implementation is
      then actually used and where, is left to the programmer and her/his
      use of the package system.</p>


    <h3>Example</h3>

    <p>As an example, testing for the presence of CDR&nbsp;10 will be done
      as follows:
      <pre>
        
        <b>#+cdr-10</b> (abi-version)
        
      </pre>
    </p>


    <h3>CDR's Current State</h3>

    <p>The current set of CDR's is listed at the site
      <a href='http://cdr.common-lisp.net' target=_blank>http://cdr.common-lisp.dev</a>.  The
      following keywords are thus assigned to each of the finalized
      CDR's:<p>

    <p>
      <code>:cdr-0</code>,
      <code>:cdr-1</code>,
      <code>:cdr-2</code>,
      <code>:cdr-3</code>,
      <code>:cdr-4</code>,
      <code>:cdr-5</code>,
      <code>:cdr-6</code>,
      <code>:cdr-7</code>,
      <code>:cdr-8</code>,
      <code>:cdr-9</code>,
      <code>:cdr-10</code>,
      <code>:cdr-11</code>,
      <code>:cdr-12</code>,
      <code>:cdr-13</code>.
    </p>


    <h1>Acknowledgements</h1>

    <p>The CDR editors, and the participants to the CDR
      &quot;side&quot;-meeting at the European Lisp Symposium in Madrid,
      June 4, 2013 (ELS&nbsp;2013).</p>


    <h1>References</h1>

    <ul>
      <li><a name='ANSIHyperSpec'>[1]</a>
        <i>The <strong>Common Lisp</strong> Hyperspec</i>,
        published online at
        <a href='http://www.lispworks.com/documentation/HyperSpec/Front/'
           target=_blank>
          http://www.lispworks.com/documentation/HyperSpec/Front/</a>,
        1994.
      </li>

      <li><a name='CDRsite'>[2]</a>
        <i>The CDR site</i>, at
        <a href='http://cdr.common-lisp.dev'
           target=_blank>
          http://cdr.common-lisp.dev</a>.
      </li>
    </ul>


    <h1>Copying and License</h1>

    <p>This work may be distributed and/or modified under the conditions
      of the <em>Creative Commons Attribution 3..0 Unported License</em>,
      or (at your option) any later version. The latest version of this
      license can be found at
      <a href='http://creativecommons.org/licenses/by/3.0/'
         target=_blank>http://creativecommons.org/licenses/by/3.0/</a>.</p>

    <p>The current maintainer of this work is Marco&nbsp;Antoniotti,
      <code>&lt;marco.antoniotti [a] unimib.it&gt;</code>.</p>
  </body>
</html>

<!-- end of file : CDR-features.tex -->
